package databaseEx2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.* ;

public class MainData {
	public static void main(String[] args) {
		try {
			// 1. Get connection to the database 
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/employee", "root", "");
			
			// 2. Create a statement 
			Statement stat = conn.createStatement();
			
			// 3. Execute SQL query
			ResultSet rs = stat.executeQuery("select * from employees");
			// 4. Process the result set
			
			while(rs.next()) {
				System.out.println( rs.getInt("id") + ", " + rs.getString("last_name") + ", " + rs.getString("first_name") + " " + rs.getString("email")
				+ ", department: " + rs.getString("department") + ", salary : " + rs.getBigDecimal("salary") );
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
